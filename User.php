<?php

class User extends Page
{

    const LOGIN_URL = "http://www.yeeyi.com/bbs/member.php?mod=logging&action=login&loginsubmit=yes&infloat=yes&lssubmit=yes";

    protected $username;

    protected $password;

    protected $ch;

    public function __construct($username, $password)
    {
        parent::__construct();
        $this->username = $username;
        $this->password = $password;

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_COOKIEJAR, CURRENT_USER_COOKIES_PATH);
        curl_setopt($this->ch, CURLOPT_COOKIEFILE, CURRENT_USER_COOKIES_PATH);
        curl_setopt($this->ch, CURLOPT_HEADER, false);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_AUTOREFERER, false);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 5000);
    }

    public function login()
    {

        if ($loginCheck = $this->isLogin()) {
            $this->userLogger->addDebug("Already logged in.");

            return true;
        }

        $posts['username'] = $this->username;
        $posts['password'] = $this->password;
        $hiddens = $this->_fetchLoginHiddenFields();
        $posts = array_merge($hiddens, $posts);

        curl_setopt($this->ch, CURLOPT_URL, self::LOGIN_URL);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $posts);

        curl_exec($this->ch);

        $loginCheck = $this->isLogin();

        if (!$loginCheck) {
            $this->userLogger->addError("failed to login");
            //@todo send alert email to myself.
        }

        $this->userLogger->addInfo("Successfully logged in.");

        return true;
    }

    public function isLogin()
    {

        curl_setopt($this->ch, CURLOPT_URL, self::LOGIN_URL);
        $loginHtml = curl_exec($this->ch);

        phpQuery::newDocumentHTML($loginHtml);
        $isLogin = count(pq('#loginstatus'));

        return $isLogin == 1;
    }

    public function _fetchLoginHiddenFields()
    {
        curl_setopt($this->ch, CURLOPT_URL, self::LOGIN_URL);

        $loginHtml = curl_exec($this->ch);

        phpQuery::newDocumentHTML($loginHtml);
        $hiddens = pq('input[type=hidden]');

        $_hiddens = array();

        foreach ($hiddens as $hidden) {
            $_hiddens[pq($hidden)->attr('name')] = pq($hidden)->val();
        }

        return $_hiddens;
    }
}