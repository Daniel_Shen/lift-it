<?php

class Post extends Page
{
    const FORUM_BASE_URL = 'http://www.yeeyi.com/bbs/';

    const REFRESH_ID = '#k_refresh';

    protected $user;

    protected $postId;

    protected $postUrl;

    protected $pageDom;

    protected $ch;


    public function __construct(User $user, $postId)
    {
        parent::__construct();
        $this->user = $user;
        $this->postId = $postId;
        $this->postUrl = self::FORUM_BASE_URL . 'forum.php?mod=viewthread&tid=' . $this->postId;

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $this->postUrl);
        curl_setopt($this->ch, CURLOPT_COOKIEJAR, CURRENT_USER_COOKIES_PATH);
        curl_setopt($this->ch, CURLOPT_COOKIEFILE, CURRENT_USER_COOKIES_PATH);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 5000);
    }

    public function lift()
    {
        $postHtml = curl_exec($this->ch);

        phpQuery::newDocumentHTML($postHtml);
        $refreshUrl = pq(self::REFRESH_ID)->attr('href');

        if (!$refreshUrl) {
            $this->userLogger->addError('Cannot find post refreshing url');
            return false;
        }

        $refreshUrl = self::FORUM_BASE_URL . $refreshUrl;

        curl_setopt($this->ch, CURLOPT_URL, $refreshUrl);
        $result =  curl_exec($this->ch);

        if (!$result) {
            $this->userLogger->addError('Failed to lift post  ' . $this->postId);
        }

        if ($this->hadErrors($result) === false) {
            $this->userLogger->addInfo('Successfully lifted post ' . $this->postId);
            return true;
        }

        return false;
    }
}
