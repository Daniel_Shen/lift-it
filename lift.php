<?php

date_default_timezone_set('Australia/Melbourne');

define("APP_PATH", realpath(dirname(__FILE__)));
define("VENDORS_PATH", realpath(APP_PATH . '/vendor'));
define("MY_VENDORS_PATH", realpath(APP_PATH . '/myvendors'));
define("USERS_PATH", realpath(APP_PATH . '/users'));
define("APP_LOG_FILE", APP_PATH . '/logs.log');

define("CURRENT_USER", $argv[1]);
define("CURRENT_USER_PATH", USERS_PATH . '/' . $argv[1]);
define("CURRENT_USER_CONFIG_PATH", CURRENT_USER_PATH . '/config.json');
define("CURRENT_USER_LOG_PATH", CURRENT_USER_PATH . '/logs.log');
define("CURRENT_USER_COOKIES_PATH", CURRENT_USER_PATH . '/cookies.txt');

require_once (VENDORS_PATH . '/autoload.php');
require_once(MY_VENDORS_PATH . '/phpQuery/phpQuery.php');
require_once(APP_PATH . '/Page.php');
require_once (APP_PATH . '/User.php');
require_once (APP_PATH . '/Post.php');

if (!file_exists(CURRENT_USER_PATH)) {
    exit('Error: Cannot file user folder, ' . CURRENT_USER_PATH);
}

$configFile= file_get_contents(CURRENT_USER_CONFIG_PATH);
$config = json_decode($configFile, true);

$user = new User($config['username'], $config['password']);

foreach($config['postIds'] as $postId) {
    $post = new Post($user, $postId);
    $post->lift();
}


