<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Page {

    const ERROR_ID = "#messagetext > p";

    public $appLogger;

    public $userLogger;

    public function __construct() {

        $this->appLogger = (new Logger('APPLICATION'))->pushHandler(new StreamHandler(APP_LOG_FILE, Logger::DEBUG));

        $this->userLogger = (new Logger(CURRENT_USER))->pushHandler(new StreamHandler(CURRENT_USER_LOG_PATH, Logger::DEBUG));;
    }

    public function hadErrors($html) {
        phpQuery::newDocumentHTML($html);
        $errors = pq('#messagetext.alert_error > p');

        if (count($errors)) {
            $this->userLogger->addError($errors->text());
            return true;
        }

        return false;
    }
}